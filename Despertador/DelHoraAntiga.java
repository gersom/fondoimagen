import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import IO.IO;


public class DelHoraAntiga 
{	
	//testar se a hora ou data já passou
	public static void Testar ()
	{
		int dia, mes, ano;
		int hora,minuto,segundo;
		int aux;
		
		String time = PegarHora ();
		
		//data
		aux = time.charAt(0) - 48;
		dia = aux;
		aux = time.charAt(1) - 48;
		dia = dia * 10 + aux;

		aux = time.charAt(3) - 48;
		mes = aux;
		aux = time.charAt(4) - 48;
		mes = mes * 10 + aux;

		aux = time.charAt(6) - 48;
		ano = aux;
		aux = time.charAt(7) - 48;
		ano = ano * 10 + aux;
		aux = time.charAt(8) - 48;
		ano = ano * 10 + aux;
		aux = time.charAt(9) - 48;
		ano = ano * 10 + aux;
		//fim data
		
		//hora
		hora = time.charAt(11) - 48;
		aux = time.charAt(12) - 48;
		hora = hora * 10 + aux;

		// pego o numero na posição 3
		minuto = time.charAt(14) - 48;
		// pego uma variavel auxiliar
		aux = time.charAt(15) - 48;
		// se minutos for 25, então é 2 * 10 = 20 + 5
		minuto = minuto * 10 + aux;
		// no final mostrar o numero

		segundo = time.charAt(17) - 48;
		aux = time.charAt(18) - 48;
		segundo = segundo * 10 + aux;

		//fim hora
		
		int diabd = 0, mesbd = 0, anobd = 0, horabd = 0, minutobd = 0, segundobd = 0;

		try {
			// tentar conectar ao banco
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost/calendario", "root", "xande");

			// criando statement
			Statement stmt = conn.createStatement();

			// string da pesquisa
			// ai vai pegar a data mais próxima
			String sql = "SELECT * FROM data ORDER BY ano,mes,dia,hora,minuto,segundo";

			// criando o resultset
			ResultSet rs = stmt.executeQuery(sql);

			if (rs.next()) {
				diabd = rs.getInt("dia");
				mesbd = rs.getInt("mes");
				anobd = rs.getInt("ano");
				horabd = rs.getInt("hora");
				minutobd = rs.getInt("minuto");
				segundobd = rs.getInt("segundo");

				
				System.out.println("banco de dados: "+"dia: " + diabd + " mes " + mesbd +
						" ano " + anobd + " hora " + horabd + " minuto " + minutobd +
						" segundo "	+ segundobd);
				

				// fechar a pesquisa
				rs.close();
				conn.close();
				
				
				System.out.println("data de hoje: dia: "+dia+" mes: "+mes+" ano: "+ano+
									" hora: "+hora+" minuto: "+minuto+" segundo: "+segundo);
			   
				
				//testar data e hora
				if(anobd < ano)
				{
					IO.println("deletei pelo ano");
					Despertador.deletar();
					Testar();
				}//fim se ano
				else{
					if(mesbd <mes && anobd < ano)
					{
						IO.println("deletei pelo mes e ano");
						Despertador.deletar();
						Testar();
					}//fim se mes & ano
					else{
						if(mesbd < mes && anobd == ano)
						{
							IO.println("deletei pelo mes (menor) e ano igual");
							Despertador.deletar();
							Testar();
						}//fim se mes & ano == ano
						else{
							if(diabd < dia && mesbd == mes && anobd == ano)
							{
								IO.println("deletei pelo dia menor em um mesmo mes");
								Despertador.deletar();
								Testar();
							}//fim se dia menor
						}
					}
				}//fim testes lógicos
				
				if(diabd == dia && mesbd == mes && anobd == ano)
				{
					//hora
					if(horabd < hora)
					{
						IO.println("hora menor que a outra");
						Despertador.deletar();
						Testar();
					}else{
						if(horabd == hora && minutobd < minuto)
						{
							IO.println("minuto menor que outro");
							Despertador.deletar();
							Testar();
						}else{
							if(horabd == hora && minutobd == minuto && segundobd < segundo)
							{
								IO.println("segundo menor que o outro");
								Despertador.deletar();
								Testar();
							}
						}
					}
				}
				
				//fim testar data e hora
				
			}// fim se da consulta com o bd
			
		} catch (SQLException se) {
			// erro no jdbc
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		}
		
	}//fim Testar
	
	public static String PegarHora()
	{
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy;HH:mm:ss");
		String hora = df.format(new Date());
		
		return hora;
	}

}//fim class
