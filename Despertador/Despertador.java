import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequence;
import javax.sound.midi.Sequencer;
import javax.swing.JOptionPane;

import IO.IO;

public class Despertador {
	static String mostrar_mensagem;
	static int verificar_hora, verificar_min, verificar_seg;
	static boolean rodar = true;
	static Sequencer player;

	// transforma a hora/data de string pra int
	public static int transformar(String frase, int posicao1, int posicao2) {
		int result, aux;

		aux = frase.charAt(posicao1) - 48;
		result = aux;
		aux = frase.charAt(posicao2) - 48;
		result = result * 10 + aux;

		return result;
	}

	// pegar a hora digitada pelo usuário
	/*
	public static int[] Pegar(String data, String hora) {
		int dia, mes, ano, aux;
		int horas, minuto, segundo;
		int[] usuario = new int[6];

		// data
		usuario[0] = transformar(data, 0, 1);// dia
		usuario[1] = transformar(data, 3, 4);// mes

		usuario[2] = transformar(data, 6, 7);// ano
		aux = usuario[2];
		usuario[2] = data.charAt(8) - 48;
		aux = aux * 10 + usuario[2];
		usuario[2] = data.charAt(9) - 48;
		aux = aux * 10 + usuario[2];

		// hora
		usuario[3] = transformar(hora, 0, 1);// hora
		usuario[4] = transformar(hora, 3, 4);// minuto
		usuario[5] = transformar(hora, 6, 7);// segundo
		return usuario;
	}
	*/
	
	// pegar a hora do sistema
	public static int[] PegarHora() {

		int[] sistema = new int[3];
		// obs1 no final
		// instancia a variavel df
		DateFormat df = new SimpleDateFormat("HH:mm:ss");
		// formata a hora para horas:minutos
		String hora = df.format(new Date());

		sistema[0] = transformar(hora, 0, 1);// hora
		sistema[1] = transformar(hora, 3, 4);// minuto
		sistema[2] = transformar(hora, 6, 7); // segundo

		return sistema;
	}// fim metodo PegarHora

	// classe que pega o valor do banco
	public static void Bancodedados() {
		int dia = 0, mes = 0, ano = 0, hora = 0, minuto = 0, segundo = 0;
		String mensagem;
		int[] hora_sistema = new int[3];

		// classe para deletar horas e datas que já passaram
		DelHoraAntiga.Testar();

		try {
			// tentar conectar ao banco
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost/calendario", "root", "xande");

			// criando statement
			Statement stmt = conn.createStatement();

			// string da pesquisa
			// ai vai pegar a data mais próxima
			String sql = "SELECT * FROM data ORDER BY dia,mes,ano,hora,minuto,segundo";

			// criando o resultset
			ResultSet rs = stmt.executeQuery(sql);

			if (rs.next()) {
				dia = rs.getInt("dia");
				mes = rs.getInt("mes");
				ano = rs.getInt("ano");
				hora = rs.getInt("hora");
				minuto = rs.getInt("minuto");
				segundo = rs.getInt("segundo");
				mensagem = rs.getString("mensagem");
				mostrar_mensagem = mensagem;

				/*System.out.println("dia: " + dia + " mes " + mes + " ano "
						+ ano + " hora " + hora + " minuto " + minuto
						+ " segundo " + segundo);*/
				JOptionPane.showMessageDialog(null, "carreguei uma hora, irei te lembrar"
		                ,"Carreguei", JOptionPane.PLAIN_MESSAGE);

				// fechar a pesquisa
				rs.close();
				conn.close();

				// passar data pra 3 ints

				DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
				String data = df.format(new Date());

				int diabd, mesbd, anobd, aux;

				// pelo menos na 1ª vez tem que ser só o aux senão fica null12
				// por exemplo
				diabd = transformar(data, 0, 1);

				mesbd = transformar(data, 3, 4);

				anobd = transformar(data, 6, 7);
				aux = anobd;
				anobd = data.charAt(8) - 48;
				aux = aux * 10 + anobd;
				anobd = data.charAt(9) - 48;
				aux = aux * 10 + anobd;
				anobd = aux;

				if (dia == diabd && mes == mesbd && ano == anobd) {
					// mudar as horas minutos e segundos das variaveis globais
					verificar_hora = hora;
					verificar_min = minuto;
					verificar_seg = segundo;
					hora_sistema = PegarHora();
					Relogio(hora_sistema[0], hora_sistema[1], hora_sistema[2]);
				}// fim se dia mes e ano estiverem iguais
            else
            {
               JOptionPane.showMessageDialog(null, "Não tenho nenhum horário para tocar"
						+ ", desligando","Erro", JOptionPane.PLAIN_MESSAGE);
				   rodar = false;
            }

			}// fim se da consulta com o bd
			else {
				JOptionPane.showMessageDialog(null, "Não tenho nenhum horário para tocar"
						+ ", desligando","Erro", JOptionPane.PLAIN_MESSAGE);
				rodar = false;
			}// fim else

		} catch (SQLException se) {
			// erro no jdbc
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		}
		// fim passar data pra 3 ints
	}// fim metodo Bancodedados
	
	//deleta a primeira linha da consulta, que no caso seria a
	//hora que eu não quero no momento
	public static void deletar() {
		try {
			// tentar conectar ao banco
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost/calendario", "root", "xande");

			// criando statement
			Statement stmt = conn.createStatement();

			String sql = "SELECT codigo FROM data ORDER BY dia,mes,ano,hora,minuto,segundo";

			ResultSet rs = stmt.executeQuery(sql);

			int codigo;
			String sql2 = "";

			if (rs.next()) {
				codigo = rs.getInt("codigo");
				sql2 = ("DELETE FROM data WHERE codigo = " + codigo);
			}
			// executando o delete
			stmt.executeUpdate(sql2);

			// fechar a conexão
			conn.close();
		} catch (SQLException se) {
			// erro no jdbc
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		}
	}// fim metodo deletar

	//faz um relógio e fica testando se deu a hora
	public static void Relogio(int h, int m, int s) {
		int hora = h;
		int minutos = m;
		int segundos = s;
		
		int [] horaNova;
		
		// existe um delay de +/- 1 segundo quando se pega a hora até
		// começar o relógio

		// laço de repetição
		while (rodar) {
			// IO.println("horas: " + hora + " minutos: " + minutos
			// + " segundos: " + segundos);

			if (segundos == 60) {
				segundos = 0;
				minutos++;
			}// fim se segundos

			
			//para ficar mais preciso, a cada 1 hora vou pegar a hora denovo
			if (minutos == 60) {
				horaNova = PegarHora();
				
				segundos = horaNova [2];
				minutos = horaNova [1];
				hora = horaNova[0];
				
				//System.out.println("seg: "+segundos+" minutos: "+minutos+" hora: "+hora);
			}// fim se minutos

			if (hora == 24) {
				hora = 0;
				//quando deu um novo dia, pega a hora denovo
				Bancodedados();
			}// fim se hora

			VerificaHora(hora, minutos, segundos);
			//	 System.out.println("hora: "+hora+
			// " min: "+minutos+" seg: "+segundos);
			// espere 1 segundo
			IO.pause(1000);
			segundos++;
		}
	}// fim metodo Relogio
	
	//se a hora no relógio der , executa esse método para tocar a música
	public static void VerificaHora(int hora, int min, int seg) {
		// aqui é o horario a ser testado, que está sendo pego no inicio

		// testa ele
		if ((verificar_hora == hora) && (verificar_min == min)
				&& (verificar_seg == seg)) {

			carregarMusica();
         // e aqui só toca a música ja carregada
         player.start();
			JOptionPane.showMessageDialog(null, mostrar_mensagem
	                ,"Estou te lembrando de", JOptionPane.PLAIN_MESSAGE);
			// fecha a música
			IO.pause(3500);
			player.close();

			// deletar essa hora
			deletar();

			// pega a próxima hora e continua o programa
			Bancodedados();

		}// fim se
	}// fim metodo VerificarHora

	public static void carregarMusica() {

		String musica1 = "aviso.mid"; // ISSO É SÓ A URL DA
		// NOSSA MÚSICA

		try {

			player = MidiSystem.getSequencer(); // INICIA O TOCADOR

			// CARREGA A MÚSICA A SER TOCADA
			Sequence musica = MidiSystem.getSequence(new File(musica1));

			player.open(); // ABRE O TOCADOR

			// PASSA A MÚSICA QUE VAI SER TOCADA PARA O TOCADOR
			player.setSequence(musica);

			// DEFINE QUANTAS VEZES VAI TOCAR A MÚSICA
			// OBS: 0 = 1 vez; 1 = 2 vezes; 2 = 3 vezes...
			player.setLoopCount(0);

			// COMEÇA A TOCAR.... CASO QUEIRA PARAR DE TOCAR A
			// MÚSICA BASTA: player.stop();

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Erro ao carregar ou tocar"+musica1
	                ,"Erro", JOptionPane.PLAIN_MESSAGE);
			//System.out.println("Erro ao carregar ou tocar: " + musica1);
		}// fim try

	}// fim metodo tocarMusica

	// método principal
	public static void main(String [] args) 
	{
		/*
      //carrega a música a ser tocada
		carregarMusica();
		//inicia o programa
		Bancodedados();
		//se não tiver nenhum horário para tocar, vai ficar com uma música carregada,
		//porque quando toca, ela descarrega, e carrega outra, e descarrega....
		//sucessivamente, mas essa aqui eu tiro pra não dar problema
		player.close();
      */
      
      //tirei tudo pra tentar gastar menos memória
      Bancodedados();
	}/fim main
}//fim class
