import java.awt.Event;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Editar extends JFrame implements ActionListener
{
	JTextField txtpesquisa;
	
	public Editar ()
	{
		//criando a janela
		JFrame janela = new JFrame ();
		//janela.setDefaultCloseOperation(EXIT_ON_CLOSE);
		janela.setTitle("Excluir");
		janela.setLocationRelativeTo(null);
		janela.setLayout(null);
		janela.setVisible(true);
		
		//criando componentes
		txtpesquisa = new JTextField ();
		
		JButton jbpesquisa = new JButton ("Pesquisar");
		
		JLabel jldia = new JLabel ("dia: ");
		
		//ajustar tamanho e posi��o dos componentes
		txtpesquisa.setBounds(40, 10, 100, 20);
		jbpesquisa.setBounds(150, 10, 100, 20);
		jldia.setBounds(10, 10, 30, 20);
		
		//adicionar componentes na janela
		janela.add(txtpesquisa);
		janela.add(jbpesquisa);
		janela.add(jldia);
		
		jbpesquisa.addActionListener(this);
		
		janela.setSize(270 , 320);
		
	}//fim construtor Interfece
	
	@Override
	public void actionPerformed(ActionEvent e)
	{
		if(e.getActionCommand().equals("Pesquisar"))
		{
			String dia = txtpesquisa.getText();
			Editar2 resultado = new Editar2 (dia);
		}
	}//fim actionPerformed
	
}//fim class