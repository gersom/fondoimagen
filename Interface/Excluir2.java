import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableColumnModel;

public class Excluir2 extends JFrame implements ActionListener{
	
	//objetos da tabela
		Object [][] dados;
		    
	    String [] colunas = {"Dia", "Mes", "ano", "Hora",
		    					 "Minuto", "Segundo", "Lembrete"}; 
		
		JButton btexcluir;
		
		public Excluir2 (String dia)
		{
			banco (dia);
			criaJanela ();
		}
		
		public void banco (String dia)
		{
			int num_linhas = 0;
			int dia_pesq = 0;
			
			Connection conn;
			Statement stmt;
			String sql;
			ResultSet rs;
			
			//pega o numero de registros
			
			/*
			 eu fiz isso para dar o
			 numero de linhas da tabela, por que
			 tem que reservar esse espa�o na mem�ria
			 */
			try {
				// tentar conectar ao banco
				conn = DriverManager.getConnection(
						"jdbc:mysql://localhost/calendario", "root", "xande");
				
				stmt = conn.createStatement();
				
				dia_pesq = Integer.parseInt(dia); 
				
				sql = "SELECT count(*) from data where dia = "+dia_pesq;
				
				rs = stmt.executeQuery(sql);
				
				rs.last();
				num_linhas = rs.getInt ("count(*)");
				
				conn.close();
			}
			catch(Exception ex)
			{
				JOptionPane.showMessageDialog(null, "Erro no banco de dados",
						"Erro", JOptionPane.PLAIN_MESSAGE);
			}
			
			//faz a tabela
			try
			{
				dados = new Object [num_linhas][7];
				
				conn = DriverManager.getConnection(
						"jdbc:mysql://localhost/calendario", "root", "xande");
				
				// criando statement
				stmt = conn.createStatement();

				// string da pesquisa
				// ai vai pegar a data mais pr�xima
				sql = "SELECT * FROM data where dia = "+dia_pesq+
						" ORDER BY dia,mes,ano,hora,minuto,segundo";
				
				// criando o resultset
				rs = stmt.executeQuery(sql);
				
				int linhas = 0;
				//vai criando a tabela enquanto ouver registro
				while (rs.next())
				{
					dados [linhas][0] = rs.getString("dia");
					dados [linhas][1] = rs.getString("mes");
					dados [linhas][2] = rs.getString("ano");
					dados [linhas][3] = rs.getString("hora");
					dados [linhas][4] = rs.getString("minuto");
					dados [linhas][5] = rs.getString("segundo");
					dados [linhas][6] = rs.getString("mensagem");
					linhas++;
				}
			}
			catch (Exception ex)
			{
				JOptionPane.showMessageDialog(null, "Erro no banco de dados",
						"Erro", JOptionPane.PLAIN_MESSAGE);
			}
		}//fim metodo banco
		
		public void criaJanela()
		{
			/*
			 Para poder definir o tamanho
			 m�ximo da tabela, eu coloquei
			 ela dentro de outro painel
			 (a tabela � um painel) 
			 */
			
	        JPanel painelTabela = new JPanel();
	        painelTabela.setLayout(new GridLayout(1, 1));
	        JTable tabela = new JTable(dados, colunas);
	        JScrollPane barraRolagem = new JScrollPane(tabela);
	        painelTabela.add(barraRolagem);
	        
	        JPanel painelJanela = new JPanel (); 
	        
	        //ajustando o tamanho m�ximo da tabela
	        painelTabela.setPreferredSize(new Dimension(500,300));
	        painelJanela.add(painelTabela,new GridBagConstraints());
	        
	        //criando o bot�o
	        btexcluir = new JButton ("Excluir");
	        
	        //propriedades do bot�o
	        btexcluir.setBounds(400, 308, 100, 50);
	        
	        //adicionando evento do mouse para o bot�o
	        btexcluir.addActionListener(this);
	        
	        //adicionando o bot�o na janela
	        getContentPane().add(btexcluir);
	        
	        //modelo da coluna
	        TableColumnModel modelo = tabela.getColumnModel();
	        
	        //setando o tamanho das colunas
	        //da coluna 0 at� a 3 eram 40
		     for (int i = 0; i <=3; i++)
		     {
		        modelo.getColumn(i).setMaxWidth(40);
		     }
	        modelo.getColumn(4).setMaxWidth(50);
	        modelo.getColumn(5).setMaxWidth(60);
	        
	        getContentPane().add(painelJanela);
	        //setDefaultCloseOperation(EXIT_ON_CLOSE);
	        setSize(520, 400);
	        setVisible(true);
	    }

		@Override
		public void actionPerformed(ActionEvent e)
		{
			//quando obt�o clicado
			if(e.getActionCommand().equals("Excluir"))
			{
				
			}
		}
}
