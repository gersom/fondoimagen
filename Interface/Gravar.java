import java.awt.Event;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class Gravar extends JFrame implements ActionListener {
	private JTextField txtdia, txtmes, txtano, txthora, txtminuto, txtsegundo,
			txtlembrete;

	public Gravar() {
		// criando a janela
		JFrame janela = new JFrame();
		// janela.setDefaultCloseOperation(EXIT_ON_CLOSE);
		janela.setTitle("Gravar");
		janela.setLocationRelativeTo(null);
		janela.setLayout(null);
		janela.setVisible(true);

		// cria��o dos componentes
		JButton bthoje = new JButton("Hoje");
		JButton btgravar = new JButton("Gravar");

		txtdia = new JTextField();
		txtmes = new JTextField();
		txtano = new JTextField();
		txthora = new JTextField();
		txtminuto = new JTextField();
		txtsegundo = new JTextField();
		txtlembrete = new JTextField();

		JLabel lbldia = new JLabel("dia:");
		JLabel lblmes = new JLabel("m�s:");
		JLabel lblano = new JLabel("ano:");
		JLabel lblhora = new JLabel("hora:");
		JLabel lblminuto = new JLabel("minuto:");
		JLabel lblsegundo = new JLabel("segundo:");
		JLabel lbllembrete = new JLabel("lembrete");

		// ajustar o tamanho dos componentes
		// e posi��o na tela
		bthoje.setBounds(10, 290, 100, 30);
		btgravar.setBounds(120, 290, 100, 30);

		txtdia.setBounds(120, 10, 100, 30);
		txtmes.setBounds(120, 50, 100, 30);
		txtano.setBounds(120, 90, 100, 30);
		txthora.setBounds(120, 130, 100, 30);
		txtminuto.setBounds(120, 170, 100, 30);
		txtsegundo.setBounds(120, 210, 100, 30);
		txtlembrete.setBounds(120, 250, 100, 30);

		lbldia.setBounds(10, 10, 100, 30);
		lblmes.setBounds(10, 50, 100, 30);
		lblano.setBounds(10, 90, 100, 30);
		lblhora.setBounds(10, 130, 100, 30);
		lblminuto.setBounds(10, 170, 100, 30);
		lblsegundo.setBounds(10, 210, 100, 30);
		lbllembrete.setBounds(10, 250, 100, 30);

		// adicionando o envento de clique do mouse
		bthoje.addActionListener(this);
		btgravar.addActionListener(this);

		// coloca��o dos componentes na janela
		janela.add(bthoje);
		janela.add(btgravar);

		janela.add(txtdia);
		janela.add(txtmes);
		janela.add(txtano);
		janela.add(txthora);
		janela.add(txtminuto);
		janela.add(txtsegundo);
		janela.add(txtlembrete);

		janela.add(lbldia);
		janela.add(lblmes);
		janela.add(lblano);
		janela.add(lblhora);
		janela.add(lblminuto);
		janela.add(lblsegundo);
		janela.add(lbllembrete);

		// o setSize tem que ser no final sen�o buga a tela
		janela.setSize(240, 360);
	}// fim construtor Interfece

	public String pegarDia(int num) {
		String RData = "";

		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String data = df.format(new Date());

		switch (num) {
		// dia
		case 1:
			RData = "" + data.charAt(0);
			RData = RData + "" + data.charAt(1);
			break;
		// mes
		case 2:
			RData = "" + data.charAt(3);
			RData = RData + "" + data.charAt(4);
			break;
		// ano
		case 3:
			RData = "" + data.charAt(6);
			RData = RData + "" + data.charAt(7);
			RData = RData + "" + data.charAt(8);
			RData = RData + "" + data.charAt(9);
			break;
		default:
			JOptionPane.showMessageDialog(null, "Essa Data ou hora n�o existe",
			"Erro",JOptionPane.PLAIN_MESSAGE);
			break;
		}

		return RData;
	}// fim pegarDia

	public boolean testarData()
	{
		boolean resposta = true;
		String dia = txtdia.getText(),
				   mes = txtmes.getText(),
				   ano = txtano.getText(),
				   hora = txthora.getText(),
				   minuto = txtminuto.getText(),
				   segundo = txtsegundo.getText();
		
		DateFormat df = new SimpleDateFormat ("dd/MM/yyyy");
		DateFormat df1 = new SimpleDateFormat ("HH:mm:ss"); 
		df.setLenient(false);
		df1.setLenient(false);
		try
		{
			df.parse(dia+"/"+mes+"/"+ano);
			df1.parse(hora+":"+minuto+":"+segundo);
		}
		catch(ParseException ex)
		{
			resposta = false;
		}
		
		return resposta;
	}
	
	public void gravarNoBanco() {
		String dia = txtdia.getText(),
			   mes = txtmes.getText(),
			   ano = txtano.getText(),
			   hora = txthora.getText(),
			   minuto = txtminuto.getText(),
			   segundo = txtsegundo.getText(),
			   lembrete = txtlembrete.getText();

		if (testarData()) {
			try {
				Connection conn = DriverManager.getConnection(
						"jdbc:mysql://localhost/calendario", "root", "xande");

				Statement stmt = conn.createStatement();

				String sql = ("insert into data values (" + dia + "," + mes
						+ "," + "" + ano + "," + hora + "," + minuto + ","
						+ segundo + ",null,'" + lembrete + "')");

				stmt.executeUpdate(sql);
			}// fim try
			catch (Exception e) {
				JOptionPane.showMessageDialog(null, "Erro no Banco de Dados",
						"Erro", JOptionPane.PLAIN_MESSAGE);
				e.printStackTrace();
			}
			
			JOptionPane.showMessageDialog(null, "Grava��o feita com sucesso!",
					"Mensagem", JOptionPane.PLAIN_MESSAGE);
			
		} else {
			JOptionPane.showMessageDialog(null, "Essa data n�o existe", "Erro",
					JOptionPane.PLAIN_MESSAGE);
		}//fim se
	}// fim gravarNoBanco

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("Hoje")) {
			txtdia.setText(pegarDia(1));
			txtmes.setText(pegarDia(2));
			txtano.setText(pegarDia(3));
		}

		if (e.getActionCommand().equals("Gravar")) {
			gravarNoBanco();
		}
	}// fim actionPerformed

}// fim class