import java.awt.Event;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

public class Inicio extends JFrame implements ActionListener
{
	
	public Inicio ()
	{
		//criando a janela
		JFrame janela = new JFrame ();
		janela.setDefaultCloseOperation(EXIT_ON_CLOSE);
		janela.setTitle("Inicio");
		janela.setLocationRelativeTo(null);
		janela.setLayout(null);
		janela.setVisible(true);
				
		//cria��o dos componentes
		JButton btgravar = new JButton ("Gravar");
		JButton bteditar = new JButton ("Editar");
		JButton btexcluir = new JButton ("Excluir");
		JButton btpesquisar = new JButton ("Pesquisar");
		
		//ajustar o tamanho dos componentes
		//e posi��o na tela
		btgravar.setBounds(10, 10, 200, 50);
		bteditar.setBounds(10, 80, 200, 50);
		btexcluir.setBounds(10, 150, 200, 50);
		btpesquisar.setBounds(10, 220, 200, 50);
				
		//adicionando o envento de clique do mouse
		btgravar.addActionListener(this);
		bteditar.addActionListener(this);
		btexcluir.addActionListener(this);
		btpesquisar.addActionListener(this);
		
		//coloca��o dos componentes na janela
		janela.add(btgravar);
		janela.add(bteditar);
		janela.add(btexcluir);
		janela.add(btpesquisar);
		
		//o setSize tem que ser por �ltimo sen�o buga
		janela.setSize(240 , 320);
	}//fim construtor Interfece
	
	public static void main (String [] args)
	{
		Inicio janela = new Inicio ();
	}//fim main
	
	@Override
	public void actionPerformed(ActionEvent e)
	{
		//cada bot�o abre uma janela diferente
		
		if(e.getActionCommand().equals("Gravar"))
		{
			Gravar gravar = new Gravar ();
		}
		
		if(e.getActionCommand().equals("Editar"))
		{
			Editar editar = new Editar();
		}
		
		if(e.getActionCommand().equals("Excluir"))
		{
			Excluir excluir = new Excluir ();
		}
		
		if(e.getActionCommand().equals("Pesquisar"))
		{
			Pesquisar pesquisar = new Pesquisar();
		}
	}//fim actionPerformed
	
}//fim class