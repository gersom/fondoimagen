package dalahora;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import javax.swing.Timer;
import javax.swing.JFrame;

public class relojHora extends JFrame {

   private Calendar calendar;
    private int hora, minuto, segundo, dia, mes, anio, dia_nombre;
    private final String[] SEMANA = {"Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"};
    private Timer timer;
    private ActionListener actionlistener;
    
    public relojHora() {
       calendar = Calendar.getInstance();
        actionlistener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {

            }
        };
        timer = new Timer(990, actionlistener);
        initComponents();
        obtenerDatos();
        mostrarDatos();
        campoHora.setText(getFecha());
        timer.start();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        campoHora = new java.awt.Label();
        campoFecha = new java.awt.Label();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        campoHora.setText("label1");

        campoFecha.setText("label2");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(campoHora, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addComponent(campoFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 314, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(50, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(campoHora, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(41, 41, 41)
                .addComponent(campoFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(48, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private String getFecha() {
        String fecha ;
        fecha = SEMANA[dia_nombre] + " " + dia + " de " + mes + " del " + anio;
        return fecha;
    }

    private void validarHora() {
        if (segundo == 59) {
            minuto++;
            segundo = 0;
        if (minuto == 59) {
            hora++;
            minuto = 0;
            if (hora == 23){
                hora = 0;
            }
        }
    }
        else{
          segundo++;
    }
    }
    private void mostrarDatos() {
        campoFecha.setText(hora + ":" + minuto + ":" + segundo);
    }

    private void obtenerDatos() {
        hora = calendar.get(Calendar.HOUR_OF_DAY);
        minuto = calendar.get(Calendar.MINUTE);
        segundo = calendar.get(Calendar.SECOND);
        dia = calendar.get(Calendar.DAY_OF_MONTH);
        mes = calendar.get(Calendar.MONTH) + 1;
        anio = calendar.get(Calendar.YEAR);
        dia_nombre = calendar.get(Calendar.DAY_OF_WEEK);
    }

    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private java.awt.Label campoFecha;
    private java.awt.Label campoHora;
    // End of variables declaration//GEN-END:variables
}
