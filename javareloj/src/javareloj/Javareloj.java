/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javareloj;

/**
 *
 * @author lenovo1
 */
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import javax.swing.Timer;
public class Javareloj {

    public class javareloj extends javax.swing.JFrame {

    private Calendar calendar;
    private int hora, minuto, segundo, dia, mes, anio, dia_nombre;
    private final String[] SEMANA = {"Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"};
    private Timer timer;
    private ActionListener actionlistener;

    public javareloj() {
        calendar = Calendar.getInstance();
        actionlistener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {

            }
        };
        timer = new Timer(990, actionlistener);
        initComponents();
        obtenerDatos();
        mostrarDatos();
        campoFecha.setText(getFecha());
        timer.start();
    }

    @SuppressWarnings("unchecked")

    private String getFecha() {
        String fecha ;
        fecha = SEMANA[dia_nombre] + " " + dia + " de " + mes + " del " + anio;
        return fecha;
    }

    private void validarHora() {
        if (segundo == 59) {
            minuto++;
            segundo = 0;
        if (minuto == 59) {
            hora++;
            minuto = 0;
            if (hora == 23){
                hora = 0;
            }
        }
    }
        else{
          segundo++;
    }
    }
    private void mostrarDatos() {
        campoHora.setText(hora + ":" + minuto + ":" + segundo);
    }

    private void obtenerDatos() {
        hora = calendar.get(Calendar.HOUR_OF_DAY);
        minuto = calendar.get(Calendar.MINUTE);
        segundo = calendar.get(Calendar.SECOND);
        dia = calendar.get(Calendar.DAY_OF_MONTH);
        mes = calendar.get(Calendar.MONTH) + 1;
        anio = calendar.get(Calendar.YEAR);
        dia_nombre = calendar.get(Calendar.DAY_OF_WEEK);
    }

    private javax.swing.JLabel campoFecha;
    private javax.swing.JLabel campoHora;

        private void initComponents() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }  
    }